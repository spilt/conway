# Conway
This is a program for running [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
on the command line.

![Preview](https://bitbucket.org/spilt/conway/downloads/conway.gif)

## Building
`make` to build and `make install` to install.

## Usage
Command line flags:

* `-S [0-8]*` the rules for when an alive cell survives
* `-B [0-8]*` the rules for when a new cell is born
* `-A <color>` provide a color for alive cells (RGB hex, either 3 or 6 chars)
* `-D <color>` provide a color for dead cells (RGB hex, either 3 or 6 chars)

Some interesting rulesets can be found in [this list](http://psoup.math.wisc.edu/mcell/rullex_life.html).

Actions:

* Mouse: draw cells (by default: create new cells)
* Space: toggle mouse mode between create/destroy
* `p`: toggle play/pause
* `q` or Escape: quit
* `c`: clear the world
